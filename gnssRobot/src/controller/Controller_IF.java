package controller;

import model.Robot_IF;

public interface Controller_IF {
	
	public void printRobots();
	public void getWaypoints(Robot_IF robot);
	public void realtimeListener(final Robot_IF robot);

}
