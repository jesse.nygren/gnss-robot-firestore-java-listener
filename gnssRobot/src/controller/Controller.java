package controller;

import model.CRUD_IF;
import model.Robot_IF;

public class Controller implements Controller_IF {

	private CRUD_IF model;
	
	public Controller(CRUD_IF model) {
		this.model = model;
		
	}

	@Override
	public void printRobots() {
		model.printRobots();
		
	}

	@Override
	public void getWaypoints(Robot_IF robot) {
		model.getWaypoints(robot);
		
	}

	@Override
	public void realtimeListener(Robot_IF robot) {
		model.realtimeListener(robot);
	}
	
}
