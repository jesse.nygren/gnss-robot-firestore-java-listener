package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.*;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.EventListener;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreException;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.Query.Direction;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.Nullable;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

public class FirebaseCRUD implements CRUD_IF, Runnable {

	private FileInputStream serviceAccount;
	private FirebaseOptions options;
	private Firestore db;
	private DocumentReference robotRef;
	private DocumentReference statusRef;

	private Robot_IF robot;
	private Boolean skipFirstObject = false;
	private Boolean notFirstRun = false;

	private String csvEntries = "lat,lng\n";
	private String robotCsvEntries = "lat,lng,cmps,spd,stat\n";

//	private Map<String, Object> data = new HashMap<>();
	private Map<String, Object> robotData = new HashMap<>();

	private Map<String, Object> statusData = new HashMap<>();

	private String currentWorkingPath = System.getProperty("user.dir");
	private String fileName = currentWorkingPath + File.separator + "waypoints.csv";
	private String csv = "LocSpeedCompassStatus.csv";
	private String commandCsv = "commands.csv";

	private int threadTimer;
	private int originalThreadTimer = threadTimer;

	private List<Waypoint> wpList;

	private final String command = "rosrun gnss_control wp_list_update.sh";

	// Constructor
	public FirebaseCRUD(Robot_IF robot, int threadTimer) {
		this.robot = robot;
		this.threadTimer = threadTimer;
		try {
			serviceAccount = new FileInputStream("serviceAccount/serviceAccount.json");
			options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://firebase-gnss-robot.firebaseio.com/").build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FirebaseApp.initializeApp(options);
		db = FirestoreClient.getFirestore();

		robotRef = db.collection("robots").document(robot.getName());
		statusRef = db.collection("robotStatus").document(robot.getName());

//		robotRef.set(robotData);
//		statusRef.set(statusData);

		System.out.println(robotRef.getId());

		this.robot = robot;
		this.threadTimer = threadTimer;
	}

	public void printRobots() {
		// asynchronously retrieve multiple documents
		ApiFuture<QuerySnapshot> future = db.collection("robots").get();
		// future.get() blocks on response
		List<QueryDocumentSnapshot> documents = null;
		try {
			documents = future.get().getDocuments();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (DocumentSnapshot document : documents) {
			System.out.println(document.getId());

		}
	}

	public int getStatus(Robot_IF robot) {

		int status = 0;

		ApiFuture<DocumentSnapshot> future = statusRef.get();

		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public List<Waypoint> getWaypoints(Robot_IF robot) {

		List<Waypoint> wpList = new ArrayList<Waypoint>();
		Waypoint wp;

		CollectionReference waypointCollection = db.collection("waypoints");
		Query query = waypointCollection.whereEqualTo("name", robot.getName())
				.orderBy("timestamp", Direction.DESCENDING).limit(1);
		ApiFuture<QuerySnapshot> querySnapshot = query.get();
		try {

			for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
				if (skipFirstObject == true) {
					System.out.println(document.getId());

					DocumentReference docRef = db.collection("waypoints").document(document.getId());

					ApiFuture<DocumentSnapshot> future = docRef.get();
					// ...
					// future.get() blocks on response
					DocumentSnapshot doc = future.get();
					if (doc.exists()) {
						String s = doc.getData().get("waypoint").toString();
						List<Object> objectList = (List<Object>) doc.getData().get("waypoint");

						for (Object o : objectList) {
							String parsed = o.toString().replaceAll("[={} a-z]+", "");
							String parts[] = parsed.split(",");
							Double lat = Double.parseDouble(parts[1]);
							Double lng = Double.parseDouble(parts[0]);

							wp = new Waypoint(lat, lng);
							wpList.add(wp);

						}

						return wpList;
					} else {
						System.out.println("No doc");
					}

				}
				skipFirstObject = true;
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public void listenWaypoints(final Robot_IF robot) {

		db.collection("waypoints").whereEqualTo("name", robot.getName())
				.addSnapshotListener(new EventListener<QuerySnapshot>() {
					public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirestoreException e) {
						if (e != null) {
							System.err.println("Listen failed:" + e);
							return;
						}

						wpList = getWaypoints(robot);

						if (wpList != null) {
							writeWaypointsCSV(wpList);

						}

					}
				});
	}

	public void listenStatus(final Robot_IF robot) {
		statusRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
			@Override
			public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirestoreException e) {
				if (e != null) {
					System.err.println("Listen failed: " + e);
					return;
				}

				if (snapshot != null && snapshot.exists()) {

					int status = Integer.parseInt(snapshot.getData().get("stat").toString());
					robot.setStatus(status);
					updateStatusCSV(robot.getStatus());

//					New execute location, try to use this! :)
//					executeBashCommand(readCommand(robot.getStatus()));

					System.out.println(readCommand(robot.getStatus()));
				} else {
					System.out.print("Current data: null");
				}
			}
		});
	}

	public void printWaypoints(List<Waypoint> wpList) {
		int counter = 0;
		for (Waypoint s : wpList) {
			System.out.println(counter + " Lat: " + s.getLatitude() + " Lng: " + s.getLongitude());
			counter++;
		}
	}

	private void updateStatusCSV(int status) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(csv));

			writer.write(robotCsvEntries);

			writer.write(robot.getLatitude() + "," + robot.getLongitude() + "," + robot.getCompass() + ","
					+ robot.getSpeed() + "," + status + "\n");

			System.out.println("Updated status!");

			writer.close();
// Old execute
//			executeBashCommand(command);

		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

	public void writeWaypointsCSV(List<Waypoint> wpList) {

		printWaypoints(wpList);

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

			writer.write(csvEntries);

			for (Waypoint s : wpList) {
				writer.write(s.getLatitude() + "," + s.getLongitude() + "\n");
			}

			writer.close();

//			executeBashCommand(command);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void executeBashCommand(String command) {
		try {
			Process proc = Runtime.getRuntime().exec(command);
			BufferedReader read = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			// proc.waitFor();
			while (read.ready()) {
				System.out.println(read.readLine());
			}
			read.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void run() {
		while (true) {
			readCsvStatus(csv);
		}
	}

	@SuppressWarnings("null")
	private String readCommand(int status) {

		String command = null;
		try {
			Reader reader = Files.newBufferedReader(Paths.get(commandCsv));
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

			System.out.println();
			String[] nextRecord;

			while ((nextRecord = csvReader.readNext()) != null) {
				command = nextRecord[status];
			}

			csvReader.close();

			return command;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	private void readCsvStatus(String csvPath) {

		double lat = 0, lng = 0, spd = 0;
		int cmps = 0, stat = 0;
		int gpsError = 6;
		boolean gpsOK = true;

		try {
			Reader reader = Files.newBufferedReader(Paths.get(csv));
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

			System.out.println();
			String[] nextRecord;

			while ((nextRecord = csvReader.readNext()) != null) {
				lat = Double.parseDouble(nextRecord[0]);
				lng = Double.parseDouble(nextRecord[1]);
				cmps = Integer.parseInt(nextRecord[2]);
				spd = Double.parseDouble(nextRecord[3]);
				stat = Integer.parseInt(nextRecord[4]);
			}

			if (notFirstRun) {
				gpsOK = checkGpsIntegrity(threadTimer, robot.getMaxSpeed(), robot.getLatitude(), robot.getLongitude(),
						lat, lng);
				
				// This is temporary!!! REMOVE THIS WHEN DECIDED THE FUNCTIONALITY!!!!
				System.out.println("GPS ACCURACY PROBLEM");
				gpsOK = true;
			}

			if (gpsOK) {

				robotData.put("lat", lat);
				robotData.put("lng", lng);
				robotData.put("cmps", cmps);
				robotData.put("spd", spd);
				statusData.put("stat", stat);

				if (robot.getLatitude() != lat || robot.getLongitude() != lng || robot.getCompass() != cmps
						|| robot.getSpeed() != spd) {
					robotRef.set(robotData, SetOptions.merge());
					System.out.println("Lat : " + robot.getLatitude());
					System.out.println("Lng : " + robot.getLongitude());
					System.out.println("Cmps : " + robot.getCompass());
					System.out.println("Spd : " + robot.getSpeed());
					System.out.println("Stat : " + robot.getStatus());
					System.out.println("==========================");
				}

				robot.setLatitude(lat);
				robot.setLongitude(lng);
				robot.setCompass(cmps);
				robot.setSpeed(spd);

				if (robot.getStatus() != stat) {
					statusRef.set(statusData);
				}

				threadTimer = originalThreadTimer;

			} else {
				System.out.println("Bad GPS-data");
				statusData.put("stat", gpsError);
				statusRef.set(statusData);
				threadTimer = threadTimer + originalThreadTimer;

			}

			robot.setStatus(stat);

			csvReader.close();
			Thread.sleep(threadTimer);

			robotData = new HashMap<>();
			statusData = new HashMap<>();

			if (!notFirstRun) {
				notFirstRun = true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Function that uses Harversine formula to calculate length between two points.
	// Function checks the
	// integrity and quality of gps-signal.
	private boolean checkGpsIntegrity(int refreshRate, int maxSpeed, double lastLat, double lastLng, double currLat,
			double currLng) {
		double metersPerSec = maxSpeed / 3.6;
		double refreshSeconds = refreshRate / 1000;
		double maxDistance = metersPerSec * refreshSeconds;

		double earthRadius = 6378.137; // Earth's radius in km;

		double dLat = currLat * Math.PI / 180 - lastLat * Math.PI / 180;
		double dLng = currLng * Math.PI / 180 - lastLng * Math.PI / 180;

		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lastLat * Math.PI / 180)
				* Math.cos(currLat * Math.PI / 180) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = earthRadius * c;

		double travel = d * 1000; // Robots travel in meters.

		if (travel > maxDistance) {
			return false;

		} else {
			return true;
		}

	}

}