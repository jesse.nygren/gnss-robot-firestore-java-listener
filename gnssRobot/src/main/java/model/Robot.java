package model;

public class Robot implements Robot_IF {

	private final String name;
	private double latitude, longitude;
	private int compass;
	private double speed;
	private int status;
	private final int maxSpeed;

	public Robot(String name, int maxSpeed) {
		this.name = name;
		this.maxSpeed = maxSpeed;
	}
	
	

	@Override
	public int getCompass() {
		return compass;
	}

	@Override
	public void setCompass(int compass) {
		this.compass = compass;
	}

	@Override
	public double getSpeed() {
		return speed;
	}

	@Override
	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getLatitude() {
		return latitude;
	}

	@Override
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Override
	public double getLongitude() {
		return longitude;
	}

	@Override
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public int getStatus() {
		return status;
	}

	@Override
	public void setStatus(int status) {
		this.status = status;

	}



	@Override
	public int getMaxSpeed() {
		// TODO Auto-generated method stub
		return maxSpeed;
	}
}
