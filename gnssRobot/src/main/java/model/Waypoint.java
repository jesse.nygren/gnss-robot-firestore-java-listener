package model;

public class Waypoint implements Waypoint_IF {
	
	private Double latitude;
	private Double longitude;
	
	public Waypoint(){	
	}
	
	public Waypoint(Double lat, Double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}

	@Override
	public Double getLatitude() {
		// TODO Auto-generated method stub
		return latitude;
	}

	@Override
	public Double getLongitude() {
		// TODO Auto-generated method stub
		return longitude;
	}

	@Override
	public void setLatitude(Double lat) {
		this.latitude = lat;
		
	}

	@Override
	public void setLongitude(Double lng) {
		this.longitude = lng;
		
	}

}
