package model;

import java.util.List;

public interface CRUD_IF {
	
	public void printRobots();
	public List<Waypoint> getWaypoints(Robot_IF robot);
	public void listenWaypoints(final Robot_IF robot);
	public void listenStatus(final Robot_IF robot);


}
