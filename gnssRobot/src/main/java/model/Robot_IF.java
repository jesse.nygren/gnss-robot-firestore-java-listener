package model;

public interface Robot_IF {

	public String getName();
	public double getLatitude();
	public double getLongitude();
	public int getCompass();
	public double getSpeed();
	public int getStatus();
	public int getMaxSpeed();
	
	public void setStatus(int status);
	public void setCompass(int compass);
	public void setSpeed(double speed);
	public void setLatitude(double latitude);
	public void setLongitude(double longitude);
	
	
}
