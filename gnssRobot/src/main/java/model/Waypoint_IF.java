package model;

public interface Waypoint_IF {

	public Double getLatitude();
	public Double getLongitude();
	public void setLatitude(Double lat);
	public void setLongitude(Double lng);
	public String toString();
}
