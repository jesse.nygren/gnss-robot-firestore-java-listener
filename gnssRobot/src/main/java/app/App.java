package app;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import view.GUI_IF;
import view.SwingUI;

// Main program
public class App {

	public App() {
	}

	public static void main(String[] args) {
		new SwingUI(args);
	}
}