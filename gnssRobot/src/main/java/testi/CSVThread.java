package testi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import model.Robot;
import model.Robot_IF;

public class CSVThread implements Runnable {

	private Robot_IF robot;
	private static final String csv = "Testi.csv";
	private int threadTimer;

	private DocumentReference docRef;
	private Map<String, Object> newData = new HashMap<>();

	private Firestore db;
	private FileInputStream serviceAccount;
	private FirebaseOptions options;

	Map<String, Object> initMap = new HashMap<>();

	public CSVThread(Robot_IF robot, int threadTimer) {
		this.robot = robot;
		this.threadTimer = threadTimer;

		try {
			serviceAccount = new FileInputStream("serviceAccount/serviceAccount.json");
			options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://firebase-gnss-robot.firebaseio.com/").build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FirebaseApp.initializeApp(options);
		db = FirestoreClient.getFirestore();
		db.collection("robots").document(robot.getName()).set(initMap);
		docRef = db.collection("robots").document(robot.getName());
		System.out.println(docRef.getId());
	}

	@Override
	public void run() {

		while (true) {

			readCsvStatus(csv);

		}
	}

	private void readCsvStatus(String csvPath) {

		double lat = 0, lng = 0, spd = 0;
		int cmps = 0, stat = 0;

		try {
			Reader reader = Files.newBufferedReader(Paths.get(csv));
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

			System.out.println();
			String[] nextRecord;

			while ((nextRecord = csvReader.readNext()) != null) {
				lat = Double.parseDouble(nextRecord[0]);
				lng = Double.parseDouble(nextRecord[1]);
				cmps = Integer.parseInt(nextRecord[2]);
				spd = Double.parseDouble(nextRecord[3]);
				stat = Integer.parseInt(nextRecord[4]);
			}

			newData.put("lat", lat);
			newData.put("lng", lng);
			newData.put("cmps", cmps);
			newData.put("spd", spd);
			newData.put("stat", stat);

			if (robot.getLatitude() != lat || robot.getLongitude() != lng || robot.getCompass() != cmps
					|| robot.getSpeed() != spd || robot.getStatus() != stat) {
				db.collection("robots").document(robot.getName()).set(newData, SetOptions.merge());
				System.out.println("Lat : " + robot.getLatitude());
				System.out.println("Lng : " + robot.getLongitude());
				System.out.println("Cmps : " + robot.getCompass());
				System.out.println("Spd : " + robot.getSpeed());
				System.out.println("Stat : " + robot.getStatus());
				System.out.println("==========================");
			}

			csvReader.close();
			Thread.sleep(threadTimer);
			newData = new HashMap<>();
			robot.setLatitude(lat);
			robot.setLongitude(lng);
			robot.setCompass(cmps);
			robot.setSpeed(spd);
			robot.setStatus(stat);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		Thread t1 = new Thread(new CSVThread(new Robot("csvTester"), 2000));
		t1.start();

	}

}
