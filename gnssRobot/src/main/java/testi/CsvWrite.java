package testi;

import java.io.BufferedWriter;
import java.io.FileWriter;

import model.Waypoint;

public class CsvWrite implements Runnable {

	String name;
	private String csvEntries = "lat,lng,cmps,spd,stat\n";
	
	
	public CsvWrite(String n) {
		this.name = n;
	}

	@Override
	public void run() {

		while (true) {
			try {
				double lat = (int) (Math.random() * 36.082279);
				double lng = (int) (Math.random() * 140.081883);
				int cmps = (int) (Math.random() * 360);
				double spd = (Math.random() * 100);
				int stat = 0;
				
				Thread.sleep(1000);

				BufferedWriter writer = new BufferedWriter(new FileWriter("LocSpeedCompassStatus.csv"));

				writer.write(csvEntries);
				
			
				writer.write(lat +","+lng+","+cmps+","+spd+","+stat);
//				System.out.println(lat +","+lng+","+cmps+","+spd+","+stat);
				
				writer.close();
				

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Thread t1 = new Thread(new CsvWrite("Eka"));
		t1.start();
	}

}
