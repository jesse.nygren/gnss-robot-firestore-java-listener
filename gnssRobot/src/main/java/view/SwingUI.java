package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.JLabel;

import controller.Controller;
import controller.Controller_IF;
import model.FirebaseCRUD;
import model.Robot;
import model.Robot_IF;

public class SwingUI implements GUI_IF {

	private Robot_IF robot;
	private Controller_IF controller;
	private int refreshRate = 2000;

	public SwingUI(String[] args) {
		this.robot = new Robot(readRobotName("RobotNameFile.txt"),5);
		FirebaseCRUD crud = new FirebaseCRUD(this.robot, refreshRate);
		this.controller = new Controller(crud);
		Thread t = new Thread(crud);

		if (args == null || args.length < 1 || args[0] == null) {
			run();
			t.start();
		}

	}

	@Override
	public void run() {
		createGUI();
		realtimeListener(robot);
	}

	@Override
	public void createGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Firebase Comm.");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add the ubiquitous "Hello World" label.
		JLabel label = new JLabel("This is a Firebase Communications background program.");
		frame.getContentPane().add(label);

		// Display the window.
		frame.pack();
		frame.setVisible(true);

	}

	@Override
	public void realtimeListener(Robot_IF robot) {
		controller.realtimeListener(robot);

	}

	private String readRobotName(String path) {
		String line = "";
		 try {
			line = new String(Files.readAllBytes(Paths.get(path)));
			System.out.println(line);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return line;
	}

}
