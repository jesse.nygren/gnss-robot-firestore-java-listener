package controller;

import java.util.List;

import model.CRUD_IF;
import model.Robot_IF;
import model.Waypoint;

public class Controller implements Controller_IF {

	private CRUD_IF model;
	
	public Controller(CRUD_IF model) {
		this.model = model;
		
	}


	@Override
	public void realtimeListener(Robot_IF robot) {
		model.listenWaypoints(robot);
		model.listenStatus(robot);
	}	
}
